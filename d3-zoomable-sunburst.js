// URL: https://beta.observablehq.com/@wanguan2000/d3-zoomable-sunburst
// Title: D3 Zoomable Sunburst
// Author: wanguan2000 (@wanguan2000)
// Version: 319
// Runtime version: 1

const m0 = {
  id: "53bdee69d1ff858e@319",
  variables: [
//    {
//      inputs: ["md"],
//      value: function(md) {
//        return md`
//# D3 Zoomable Sunburst
//
//This variant of a [sunburst diagram](/@mbostock/d3-sunburst), a radial orientation of D3’s [hierarchical partition layout](https://github.com/d3/d3-hierarchy/blob/master/README.md#partition), shows only two layers of the [Flare visualization toolkit](https://flare.prefuse.org) package hierarchy at a time. Click a node to zoom in, or click the center to zoom out.
//        `;
//      }
//    },
    {
      name: "chart",
      inputs: [
        "partition",
        "data",
        "d3",
        "DOM",
        "width",
        "color",
        "arc",
        "format",
        "radius"
      ],
      value: function(
        partition,
        data,
        d3,
        DOM,
        width,
        color,
        arc,
        format,
        radius
      ) {
        const root = partition(data);

        root.each(d => (d.current = d));

        const svg = d3
          .select(DOM.svg(width, width))
          .style("width", "40%")
          .style("height", "auto")
        .style("margin-left", "30%")
          .style("font", "18px sans-serif");

        const g = svg
          .append("g")
          .attr("transform", `translate(${width / 2},${width / 2})`);
            
        

        const path = g
          .append("g")
          .selectAll("path")
          .data(root.descendants().slice(1))
          .enter()
          .append("path")
          .attr("fill", d => {
            while (d.depth > 1) d = d.parent;
            return color(d.data.name);
          })
          .attr("fill-opacity", d =>
            arcVisible(d.current) ? (d.children ? 0.6 : 0.4) : 0
          )
          .attr("d", d => arc(d.current))
            .on("mouseover", function(d,event){
               // document.getElementById('trend1').hidden = true;
                //if(d.data.name == 'DIGITAL MARKETING' || d.data.name == 'ASSISTED & PREDICTIVE ANALYTICS'){
                   // d3.select("#trend1").select("svg").remove();
                   // d3.select("#trend1").remove();
                    mouseover(d);
               // }
    
                })
            .on("mouseout", function() { 
               // d3.select("#trend1").remove();
            })
            ;
            
        
        path
          .filter(d => d.children)
          .style("cursor", "pointer")
          .on("click", clicked);

        path.append("title").text(
          d =>
            console.log('title',d.data.name)
        );
            
//            g
//          .append("path")
//          .attr("d", "M300,200 h-150 a150,150 0 1,0 150,-150 z")
//          .attr("fill", "red")
//          .attr("stroke", "blue")
        var wordList = []
            
        const label = g
          .append("g")
          .attr("pointer-events", "none")
          .attr("text-anchor", "middle")
          .style("user-select", "none")
        
//        .append("svg:image")
//            .attr('x', 0)
//            .attr('y', 0)
//            .attr('width', 50)
//            .attr('height', 54)
//            .attr("xlink:href", "images/logo.png")
        
          .selectAll("text")
          .data(root.descendants().slice(1))
          .enter()
          .append("text")
//            .attr("class", "wrap")
//            .attr("id", "labelWrap")
//                .attr("x", "0")
//                .attr("y", "0")
        .attr("id", d=>{
            var removeSpl = d.data.name.replace(/[^a-zA-Z ]/g, "")
            var splitName = removeSpl.split(' ')
            var getId=''
            splitName.map(item=>{
                getId = getId + item
            })
            wordList.push({'name':d.data.name,'id':getId})
            return getId;
        })
              .attr("dy", "0.35em")
             .attr("fill", "white")
              .attr("fill-opacity", d => +labelVisible(d.current))
            .attr("transform", d => labelTransform(d.current))
            .attr("text-anchor", "middle")
             .text(d =>{ 
                 d.data.name
             })
        .call(wrap, 30);
            // .text("sdjsdjbcjsdbcksdbkcsdjkcjksdvkjsdkvskdvkj")
        
//          .append('tspan')
//          .text(d => d.data.name1)
//          //.text("span1")
//          .attr("x", "0")
//          .attr("y", "0")
//          .attr("dy", "0.1em")
////          .attr("alignment-baseline", "middle")
////          .attr("text-anchor", "middle")
////          .attr("fill-opacity", d => +labelVisible(d.current))
////        .attr("transform", d => labelTransform(d.current))
//        
//          .append('tspan')
//          .text(d => d.data.name2)
//          //.text("span2")
//          .attr("x", "0")
//          .attr("y", "0")
//          .attr("dy", "1.2em")
////            .attr("alignment-baseline", "middle")
////          .attr("text-anchor", "middle")
////        .attr("fill-opacity", d => +labelVisible(d.current))
////        .attr("transform", d => labelTransform(d.current))
//        
//         .append('tspan')
//          .text(d => d.data.name3)
//          //.text("span2")
//          .attr("x", "0")
//          .attr("y", "0")
//          .attr("dy", "2.2em")
////            .attr("alignment-baseline", "middle")
////          .attr("text-anchor", "middle")
////        .attr("fill-opacity", d => +labelVisible(d.current))
////        .attr("transform", d => labelTransform(d.current))
            
                     
        const parent = g
          .append("circle")
          .datum(root)
          .attr("r", radius)
          .attr("fill", "none")
          .attr("pointer-events", "all")
          .on("click", clicked);
            
            
            
//        d3plus.textwrap()
//            .container(d3.select("#labelWrap"))
//            //.resize(true)
//            .draw();
            
            
        //////////////// Mouse over function for hover charts ///////////////////
        console.log('word list',wordList)
        setTimeout(()=>{
          wordList.map(item=>{
            var removeSpl = item.name.replace(/[^a-zA-Z ]/g, "")
            var splitName = removeSpl.split(' ')
            splitName.map((name,index)=>{                
//                var tspan = document.createElement('tspan')
//                var textnode = document.createTextNode(name);         // Create a text node
//                tspan.appendChild(textnode);  
//                document.getElementById(item.id).appendChild(tspan)
//                tspan.setAttribute('x',0)
//                tspan.setAttribute('y',0)
//                tspan.setAttribute('dy',(0.35+index)+'em')
                if(d3.select('#'+item.id) != undefined) {
                console.log(d3.select('#'+item.id))                
                var getTextArea = d3.select('#'+item.id)
                getTextArea.append('tspan')
                .attr('x',0)
                .attr('y',0)
                .attr('dy',(0.35+index)+'em')
                .text(name)
                }
            })
            
        })  
        },1000)
        
        function wrap (text, width) {

  text.each(function() {

    var breakChars = ['/', '&', '-', ' '],
      text = d3.select(this),
      textContent = text.text(),
      spanContent;

    breakChars.forEach(char => {
      // Add a space after each break char for the function to use to determine line breaks
      textContent = textContent.replace(char, char + ' ');
    });

    var words = textContent.split(/\s+/).reverse(),
      word,
      line = [],
      lineNumber = 0,
      lineHeight = 1.1, // ems
      x = text.attr('x'),
      y = text.attr('y'),
      dy = parseFloat(text.attr('dy') || 0),
      tspan = text.text(null).append('tspan').attr('x', x).attr('y', y).attr('dy', dy + 'em');

    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(' '));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        spanContent = line.join(' ');
        breakChars.forEach(char => {
          // Remove spaces trailing breakChars that were added above
          spanContent = spanContent.replace(char + ' ', char);
        });
        tspan.text(spanContent);
        line = [word];
        tspan = text.append('tspan').attr('x', x).attr('y', y).attr('dy', ++lineNumber * lineHeight + dy + 'em').text(word);
      }
    }
  });

}
            
        function mouseover(d){ 
            console.log(d)
            var var_keyword = d.data.name  
            console.log("Inside mouseover, Keyword: ",var_keyword);
            var keyword = var_keyword;
            var datapath;
            
            if(keyword == 'DIGITAL MARKETING')  {
                datapath = "data/DIGITAL_MARKETING.json";
            }
            if(keyword == 'ASSISTED & PREDICTIVE ANALYTICS')  {
                datapath = "data/ASSISTED_PREDICTIVE_ANALYTICS.json";
            }
            if(keyword == 'DIGITAL ROUTE TO MARKET')  {
                datapath = "data/DIGITAL_ROUTE_TO_MARKET.json";
            }
            if(keyword == 'E2E DATA MANAGEMENT')  {
                datapath = "data/E2E_DATA_MANAGEMENT.json";
            }
            if(keyword == 'DIGITAL SUPPLY CHAIN')  {
                datapath = "data/DIGITAL_SUPPLY_CHAIN.json";
            }
            if(keyword == 'E2E INTEGRATED OPS')  {
                datapath = "data/E2E_INTEGRATED_OPS.json";
            }
            
            if(keyword == 'Data-Led Marketing')  {
                datapath = "data/Data_Led_Marketing.json";
            }
            if(keyword == 'Agile Innovation')  {
                datapath = "data/Agile_Innovation.json";
            }
//            else {
//                return 0;
//            }
             
            
            d3.json(datapath).then(function(dataset) {
                
            console.log(dataset);
            d3.select("#trend1").select("svg").remove();

            var width = 300;
            var height = 300;
            var margin = {top: 50, right: 50, bottom: 50, left: 50};
                             
           //var n = dataset.length;
            var x_min = d3.min(dataset, function(d) { return d.x_value; });
            var x_max = d3.max(dataset, function(d) { return d.x_value; });
            var y_min = d3.min(dataset, function(d) { return d.y_value; });
            var y_max = d3.max(dataset, function(d) { return d.y_value; });

            // 5. X scale will use the index of our data
            var xScale = d3.scaleLinear()
                .domain([0, x_max]) // input
                .range([0, width]); // output

            // 6. Y scale will use the randomly generate number 
            var yScale = d3.scaleLinear()
                .domain([0, y_max]) // input 
                .range([height, 0]); // output 

            // 7. d3's line generator
            var line = d3.line()
                .x(function(d) { return xScale(d.x_value); }) // set the x values for the line generator
                .y(function(d) { return yScale(d.y_value); }) // set the y values for the line generator 
                .curve(d3.curveMonotoneX) // apply smoothing to the line

            // 8. An array of objects of length N. Each object has key -> value pair, the key being "y" and the value is a random number
            //var dataset = d3.range(n).map(function(d) { return {"y": d3.randomUniform(1)() } })

            // 1. Add the SVG to the page and employ #2
            var svg_trend = d3.select("#trend1").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
              .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
            
            

            // 3. Call the x axis in a group tag
            svg_trend.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                //.attr("color","white")
                .call(d3.axisBottom(xScale)); // Create an axis component with d3.axisBottom

            // 4. Call the y axis in a group tag
            svg_trend.append("g")
                .attr("class", "y axis")
                //.attr("color","white")
                .call(d3.axisLeft(yScale)); // Create an axis component with d3.axisLeft

            // 9. Append the path, bind the data, and call the line generator 
            svg_trend.append("path")
                .datum(dataset) // 10. Binds data to the line 
                .attr("class", "line") // Assign a class for styling 
                .attr("d", line); // 11. Calls the line generator 

            // 12. Appends a circle for each datapoint 
            svg_trend.selectAll(".dot")
                .data(dataset)
              .enter().append("circle") // Uses the enter().append() method
                .attr("class", "dot") // Assign a class for styling
                .attr("cx", function(d, i) { return xScale(d.x_value) })
                .attr("cy", function(d) { return yScale(d.y_value) })
                .attr("r", 5)
                  .on("mouseover", function(a, b, c) { 
                       // console.log(a) 
                    this.attr('class', 'focus')
                    })
                  .on("mouseout", function() {  })
                            
                });     
            }
            

        function clicked(p) { console.log(p);
            
//            console.log(p.children[1].parent.data.name);                 
//                if(p.children[1].parent.data.name == 'Assisted and Predictive Decision Making'){
//                   // alert("Data led clicked");
//                    window.open("https://unilever-my.sharepoint.com/:b:/p/jyothi_shanbhag/Ea_AF1cEq6VEiGx5-_5ggU4B5zhaiARi9geE3SlX_JyOKA?e=nmLTY5");
//                }
                  
          parent.datum(p.parent || root);

          console.log(root);
          root.each(
            d =>
              (d.target = {
                x0:
                  Math.max(0, Math.min(1, (d.x0 - p.x0) / (p.x1 - p.x0))) *
                  2 *
                  Math.PI,
                x1:
                  Math.max(0, Math.min(1, (d.x1 - p.x0) / (p.x1 - p.x0))) *
                  2 *
                  Math.PI,
                y0: Math.max(0, d.y0 - p.depth),
                y1: Math.max(0, d.y1 - p.depth)
              })
          );

          const t = g.transition().duration(750);

          // Transition the data on all arcs, even the ones that aren’t visible,
          // so that if this transition is interrupted, entering arcs will start
          // the next transition from the desired position.
          path
            .transition(t)
            .tween("data", d => {
              const i = d3.interpolate(d.current, d.target);
              return t => (d.current = i(t));
            })
            .filter(function(d) {
              return +this.getAttribute("fill-opacity") || arcVisible(d.target);
            })
            .attr("fill-opacity", d =>
              arcVisible(d.target) ? (d.children ? 0.6 : 0.4) : 0
            )
            .attrTween("d", d => () => arc(d.current));

          label
            .filter(function(d) {
                      console.log("d", d);
                      console.log(+this.getAttribute("fill-opacity") || labelVisible(d.target));
              return ( 
                +this.getAttribute("fill-opacity") || labelVisible(d.target)
              );
            })
            .transition(t)
            .attr("fill-opacity", d => +labelVisible(d.target))
            .attrTween("transform", d => () => labelTransform(d.current));
                             
            
        }

        function arcVisible(d) {
          return d.y1 <= 3 && d.y0 >= 1 && d.x1 > d.x0;
        }

        function labelVisible(d) {
          return d.y1 <= 3 && d.y0 >= 1 && (d.y1 - d.y0) * (d.x1 - d.x0) > 0.03;
        }

        function labelTransform(d) {
          const x = (((d.x0 + d.x1) / 2) * 180) / Math.PI;
          const y = ((d.y0 + d.y1) / 2) * radius;
          return `rotate(${x - 90}) translate(${y},0) rotate(${
            x < 180 ? 0 : 180
          })`;
        }

        return svg.node();
      }
    },
    {
      name: "data",
      inputs: ["require"],
      value: function(require) {
        return {
          "name": "flare",
          "children": [
           {
            "name": "DIGITAL MARKETING",
               "name1": "DIGITAL",
               "name2": "MARKETTING",
               "name3": "",
            "children": [
             {
              "name": "Data-Led Marketing",
                 "name1": "Data-Led",
                   "name2": "Marketing",
                   "name3": "",
              "children": [
               {"name": "DIGITAL DATA",
                    "name1": "DIGITAL",
                   "name2": "DATA",
                   "name3": "",
                    "children": [
                        {"name": "Data Capture", "name1": "Data", "name2": "Capture", "name3": "", "size": 10},
                        {"name": "Data Analysis", "name1": "Data", "name2": "Analysis", "name3": "", "size": 10},
                        {"name": "Data Leverage", "name1": "Data", "name2": "Leverage", "name3": "", "size": 10},
                        {"name": "Adoption", "name1": "Adoption", "name2": "", "name3": "", "size": 10}    
                    ]

               },
               {"name": "PERSONAL DATA", "name1": "PERSONAL", "name2": "DATA", "name3": "", "size": 10}
              ]
             },
             {
              "name": "Agile Innovation",
                 "name1": "Agile",
                   "name2": "Innovation",
                   "name3": "",
              "children": [
               {"name": "BetweennessCentrality", "name1": "BetweennessCentrality", "name2": "", "name3": "", "size": 10},
               {"name": "LinkDistance", "name1": "LinkDistance", "name2": "", "name3": "", "size": 10},
               {"name": "MaxFlowMinCut", "name1": "MaxFlowMinCut", "name2": "", "name3": "", "size": 10},
               {"name": "ShortestPaths", "name1": "ShortestPaths", "name2": "", "name3": "", "size": 10},
               {"name": "SpanningTree", "name1": "SpanningTree", "name2": "", "name3": "", "size": 10}
              ]
             }
            ]
           },
           
           {
            "name": "DIGITAL SUPPLY CHAIN",
               "name1": "DIGITAL",
                "name2": "SUPPLY",
                "name3": "CHAIN",
            "children": [
             {
              "name": "E2E Real Time Visibility and Analytics",
                 "name1": "E2E Real Time",
                   "name2": "Visibility and",
                   "name3": "Analytics",
              "children": [
               {"name": "AgglomerativeCluster", "name1": "AgglomerativeCluster", "name2": "", "name3": "", "size": 12},
               {"name": "CommunityStructure", "name1": "CommunityStructure", "name2": "", "name3": "", "size": 12},
               {"name": "HierarchicalCluster", "name1": "HierarchicalCluster", "name2": "", "name3": "", "size": 12},
               {"name": "MergeEdge", "name1": "MergeEdge", "name2": "", "name3": "", "size": 12}
              ]
             },
             {
              "name": "Physical Automation and RPA",
                 "name1": "Physical",
                   "name2": "Automation",
                   "name3": "and RPA",
              "children": [
               {"name": "BetweennessCentrality", "name1": "BetweennessCentrality", "name2": "", "name3": "", "size": 10},
               {"name": "LinkDistance", "name1": "LinkDistance", "name2": "", "name3": "", "size": 10},
               {"name": "MaxFlowMinCut", "name1": "MaxFlowMinCut", "name2": "", "name3": "", "size": 10},
               {"name": "ShortestPaths", "name1": "ShortestPaths", "name2": "", "name3": "", "size": 10},
               {"name": "SpanningTree", "name1": "SpanningTree", "name2": "", "name3": "", "size": 10}
              ]
             }
            ]
           },
           
           
           {
            "name": "E2E DATA MANAGEMENT",
               "name1": "E2E",
                "name2": "DATA",
                "name3": "MANAGEMENT",
            "children": [
             {
              "name": "Master Data Management",
                 "name1": "Master",
                "name2": "Data",
                "name3": "Management",
              "children": [
               {"name": "AgglomerativeCluster", "name1": "AgglomerativeCluster", "name2": "", "name3": "", "size": 12},
               {"name": "CommunityStructure", "name1": "CommunityStructure", "name2": "", "name3": "", "size": 12},
               {"name": "HierarchicalCluster", "name1": "HierarchicalCluster", "name2": "", "name3": "", "size": 12},
               {"name": "MergeEdge", "name1": "MergeEdge", "name2": "", "name3": "", "size": 12}
              ]
             },
             {
              "name": "Product Data Management",
                 "name1": "Product",
                "name2": "Data",
                "name3": "Management",
              "children": [
               {"name": "BetweennessCentrality", "name1": "BetweennessCentrality", "name2": "", "name3": "", "size": 10},
               {"name": "LinkDistance", "name1": "LinkDistance", "name2": "", "name3": "","size": 10},
               {"name": "MaxFlowMinCut", "name1": "MaxFlowMinCut", "name2": "", "name3": "", "size": 10},
               {"name": "ShortestPaths", "name1": "ShortestPaths", "name2": "", "name3": "", "size": 10},
               {"name": "SpanningTree", "name1": "SpanningTree", "name2": "", "name3": "", "size": 10}
              ]
             }
            ]
           },
           {
            "name": "ASSISTED & PREDICTIVE ANALYTICS",
               "name1": "ASSISTED &",
                "name2": "PREDICTIVE",
                "name3": "ANALYTICS",
            "children": [
             {
              "name": "Intelligent Automation at Scale (RPA)",
                 "name1": "Intelligent",
                "name2": "Automation at",
                "name3": "Scale (RPA)",
              "children": [
               {"name": "AgglomerativeCluster", "name1": "AgglomerativeCluster", "name2": "", "name3": "", "size": 12},
               {"name": "CommunityStructure", "name1": "CommunityStructure", "name2": "", "name3": "", "size": 12},
               {"name": "HierarchicalCluster", "name1": "HierarchicalCluster", "name2": "", "name3": "", "size": 12},
               {"name": "MergeEdge", "name1": "MergeEdge", "name2": "", "name3": "", "size": 12}
              ]
             },
             {
              "name": "Assisted and Predictive Decision Making",
                 "name1": "Assisted and",
                "name2": "Predictive",
                "name3": "Decision Making",
              "children": [
               {"name": "100 CCBTS LIVE WITH 90% USAGE",
                    "name1": "100 CCBTS",
                   "name2": "LIVE WITH",
                   "name3": "90% USAGE",
                    "children": [
                        {"name": "Data Capture", "name1": "Data", "name2": "Capture", "name3": "", "size": 10},
                        {"name": "Data Analysis", "name1": "Data", "name2": "Analysis", "name3": "", "size": 10},
                        {"name": "Data Leverage", "name1": "Data", "name2": "Leverage", "name3": "", "size": 10},
                        {"name": "Adoption", "name1": "Adoption", "name2": "", "name3": "", "size": 10}    
                    ]

               }
                
              ]
             }
            ]
           },
           {
            "name": "DIGITAL ROUTE TO MARKET",
               "name1": "DIGITAL",
                "name2": "ROUTE",
                "name3": "TO MARKET",
            "children": [
             {
              "name": "Digitising General trade (B2B)",
                 "name1": "Digitising",
                "name2": "General trade",
                "name3": "(B2B)",
              "children": [
               {"name": "AgglomerativeCluster", "name1": "AgglomerativeCluster", "name2": "", "name3": "", "size": 8},
               {"name": "CommunityStructure", "name1": "CommunityStructure", "name2": "", "name3": "", "size": 8},
               {"name": "HierarchicalCluster", "name1": "HierarchicalCluster", "name2": "", "name3": "", "size": 8},
               {"name": "MergeEdge", "name1": "MergeEdge", "name2": "", "name3": "", "size": 8}
              ]
             },
             {
              "name": "Business to Consumer (B2C)",
                 "name1": "Business",
                "name2": "to Consumer",
                "name3": "(B2C)",
              "children": [
               {"name": "BetweennessCentrality", "name1": "BetweennessCentrality", "name2": "", "name3": "", "size": 6},
               {"name": "LinkDistance", "name1": "LinkDistance", "name2": "", "name3": "", "size": 6},
               {"name": "MaxFlowMinCut", "name1": "MaxFlowMinCut", "name2": "", "name3": "", "size": 6},
               {"name": "ShortestPaths", "name1": "ShortestPaths", "name2": "", "name3": "", "size": 6},
               {"name": "SpanningTree", "name1": "SpanningTree", "name2": "", "name3": "", "size": 6}
              ]
             },
             {
              "name": "E-Commerce & Future-fit Channels",
                 "name1": "E-Commerce &",
                "name2": "Future-fit",
                "name3": "Channels",
              "children": [
               {"name": "AspectRatioBanker", "name1": "AspectRatioBanker", "name2": "", "name3": "", "size": 32}
              ]
             }
            ]
           },
            {
            "name": "E2E INTEGRATED OPS",
                "name1": "E2E",
                "name2": "INTEGRATED",
                "name3": "OPS",
            "children": [
             {
              "name": "SMART (NO TOUCH) S&OP",
                 "name1": "SMART",
                "name2": "(NO TOUCH)",
                "name3": "S&OP",
              "children": [
               {"name": "AgglomerativeCluster", "name1": "AgglomerativeCluster", "name2": "", "name3": "", "size": 25},
               {"name": "CommunityStructure", "name1": "CommunityStructure", "name2": "", "name3": "", "size": 25},
               {"name": "HierarchicalCluster", "name1": "HierarchicalCluster", "name2": "", "name3": "", "size": 25},
               {"name": "MergeEdge", "name1": "MergeEdge", "name2": "", "name3": "", "size": 25}
              ]
             }
            ]
           }
           
           
          ]
         }
      }
    },
    {
      name: "partition",
      inputs: ["d3"],
      value: function(d3) {
        return data => {
          const root = d3
            .hierarchy(data)
            .sum(d => d.size)
            .sort((a, b) => b.value - a.value);
          return d3.partition().size([2 * Math.PI, root.height + 1])(root);
        };
      }
    },
    {
      name: "color",
      inputs: ["d3", "data"],
      value: function(d3, data) {
        return d3
          .scaleOrdinal()
         // .range(d3.quantize(d3.interpolateRainbow, data.children.length + 1));
          .range(["#34A4C2", "#002060", "#00B0F0", "#2E75B6", "#61E0D5", "#7030A0"]);
      }
    },
    {
      name: "format",
      inputs: ["d3"],
      value: function(d3) {
        return d3.format(",d");
      }
    },
    {
      name: "width",
      value: function() {
        return 932;
      }
    },
    {
      name: "radius",
      inputs: ["width"],
      value: function(width) {
        return width / 6;
      }
    },
    {
      name: "arc",
      inputs: ["d3", "radius"],
      value: function(d3, radius) {
        return d3
          .arc()
          .startAngle(d => d.x0)
          .endAngle(d => d.x1)
          .padAngle(d => Math.min((d.x1 - d.x0) / 2, 0.005))
          .padRadius(radius * 1.5)
          .innerRadius(d => d.y0 * radius)
          .outerRadius(d => Math.max(d.y0 * radius, d.y1 * radius - 1));
      }
    },
    {
      name: "d3",
      inputs: ["require"],
      value: function(require) {
        return require("d3@5");
      }
    }
  ]
};
const notebook = {
  id: "53bdee69d1ff858e@319",
  modules: [m0]
};


export default notebook;





//////////////////// Gauge chart //////////////////////

    var gauge = function(container, configuration) {
        var that = {};
        var config = {
            size						: 200,
            clipWidth					: 210,
            clipHeight					: 110,
            ringInset					: 20,
            ringWidth					: 20,

            pointerWidth				: 10,
            pointerTailLength			: 5,
            pointerHeadLengthPercent	: 0.9,

            minValue					: 0,
            maxValue					: 3,

            minAngle					: -90,
            maxAngle					: 90,

            transitionMs				: 750,

            majorTicks					: 3,
            labelFormat					: d3.format(',g'),
            labelInset					: 10,

            arcColorFn					: d3.interpolateHsl(d3.rgb('#e8e2ca'), d3.rgb('#3e6c0a'))
        };
        console.log(d3.interpolateHsl(d3.rgb('#e8e2ca'), d3.rgb('#3e6c0a')))
        var range = undefined;
        var r = undefined;
        var pointerHeadLength = undefined;
        var value = 0;

        var svg = undefined;
        var arc = undefined;
        var scale = undefined;
        var ticks = undefined;
        var tickData = undefined;
        var pointer = undefined;

        var donut = d3.layout.pie();

        function deg2rad(deg) {
            return deg * Math.PI / 180;
        }

        function newAngle(d) {
            var ratio = scale(d);
            var newAngle = config.minAngle + (ratio * range);
            return newAngle;
        }

        function configure(configuration) {
            var prop = undefined;
            for ( prop in configuration ) {
                config[prop] = configuration[prop];
            }

            range = config.maxAngle - config.minAngle;
            r = config.size / 2;
            pointerHeadLength = Math.round(r * config.pointerHeadLengthPercent);

            // a linear scale that maps domain values to a percent from 0..1
            scale = d3.scale.linear()
                .range([0,1])
                .domain([config.minValue, config.maxValue]);

            ticks = scale.ticks(config.majorTicks);
            tickData = d3.range(config.majorTicks).map(function() {return 1/config.majorTicks;});

            arc = d3.svg.arc()
                .innerRadius(r - config.ringWidth - config.ringInset)
                .outerRadius(r - config.ringInset)
                .startAngle(function(d, i) {
                    var ratio = d * i;
                    return deg2rad(config.minAngle + (ratio * range));
                })
                .endAngle(function(d, i) {
                    var ratio = d * (i+1);
                    return deg2rad(config.minAngle + (ratio * range));
                });
        }
        that.configure = configure;

        function centerTranslation() {
            return 'translate('+r +','+ r +')';
        }

        function isRendered() {
            return (svg !== undefined);
        }
        that.isRendered = isRendered;

        function render(newValue) {
            svg = d3.select(container)
                .append('svg:svg')
                    .attr('class', 'gauge')
                    .attr('width', config.clipWidth)
                    .attr('height', config.clipHeight);

            var centerTx = centerTranslation();

            var arcs = svg.append('g')
                    .attr('class', 'arc')
                    .attr('transform', centerTx);

            arcs.selectAll('path')
                    .data(tickData)
                .enter().append('path')
                    .attr('fill', function(d, i) {
                        console.log('arc index',i)
                        var arcColor = ["#9ebf24","#f3bb26","#fe2c29"]
                        return arcColor[i];
                    })
                    .attr('stroke-width','1.5')
                    .attr('stroke','#fff')
                    .attr('d', arc);

            var lg = svg.append('g')
                    .attr('class', 'label')
                    .attr('transform', centerTx);
            lg.selectAll('text')
                    .data(ticks)
                .enter().append('text')
                    .attr('transform', function(d) {
                console.log(d)
                        var ratio = scale(d);
                        var newAngle = config.minAngle + (ratio * range);
                        if(d==100){
                            return 'rotate(' +newAngle +') translate(-25,' +((config.labelInset - r)+5) +')';
                        } else if(d==50) {
                        return 'rotate(' +newAngle +') translate(-8,' +((config.labelInset - r)+5) +')';
                        } else {
                        return 'rotate(' +newAngle +') translate(0,' +((config.labelInset - r)+5) +')';
                        }
                    })
                    .text(config.labelFormat);

            var lineData = [ [(config.pointerWidth / 2.5), 0], 
                            [0, -pointerHeadLength+15],
                            [-(config.pointerWidth / 2.5), 0],
                            [0, config.pointerTailLength],
                            [config.pointerWidth / 2.5, 0] ];
            var pointerLine = d3.svg.line().interpolate('monotone');
            var pg = svg.append('g').data([lineData])
                    .attr('class', 'pointer')
                    .attr('transform', centerTx);

            pointer = pg.append('path')
                .attr('d', pointerLine/*function(d) { return pointerLine(d) +'Z';}*/ )
                .attr('transform', 'rotate(' +config.minAngle +')');

            update(newValue === undefined ? 0 : newValue);
        }
        that.render = render;

        function update(newValue, newConfiguration) {
            if ( newConfiguration  !== undefined) {
                configure(newConfiguration);
            }
            var ratio = scale(newValue);
            var newAngle = config.minAngle + (ratio * range);
            pointer.transition()
                .duration(config.transitionMs)
                .ease('elastic')
                .attr('transform', 'rotate(' +newAngle +')');
        }
        that.update = update;

        configure(configuration);

        return that;
    };



/////////// Gauge1 ////////////
var gaugeList = [{
    name:'list 1',
    id: 1
},
{
    name:'list 2',
    id: 2
},
{
    name:'list 3',
    id: 3
},
{
    name:'list 4',
    id: 4
},
{
    name:'list 5',
    id: 5
},
{
    name:'list 6',
    id: 6
}]
 function onDocumentReady(item) {
    var name = 'power-gauge' + item.id
     var td = document.createElement("td");
     var div = document.createElement("div")
     div.setAttribute('id',name)
     td.appendChild(div)
     document.getElementById("gaugeTable-row").appendChild(td);
        var powerGauge = gauge('#'+name, {
            size: 200,
            clipWidth: 210,
            clipHeight: 120,
            ringWidth: 50,
            maxValue: 100,
           transitionMs: 4000,
        });
        powerGauge.render();
//             function updateReadings() {
//            // just pump in random data here...
//            powerGauge.update(25);
//        }
//
//        // every few seconds update reading values
//        updateReadings();
    }

    if ( !window.isLoaded ) {
        window.addEventListener("load", function() {
            gaugeList.map(item=>{
                   onDocumentReady(item); 
            })
            
        }, false);
    }


    /////////// Gauge2 ////////////
//    function onDocumentReady2() {
//        var powerGauge = gauge('#power-gauge2', {
//            size: 200,
//            clipWidth: 210,
//            clipHeight: 120,
//            ringWidth: 50,
//            maxValue: 6,
//            //transitionMs: 4000,
//        });
//        powerGauge.render();
//
//        function updateReadings() {
//            // just pump in random data here...
//            powerGauge.update(Math.random() * 10);
//        }
//
//        // every few seconds update reading values
//        updateReadings();
//        setInterval(function() {
//            updateReadings();
//        }, 5 * 1000);
//    }
//
//    if ( !window.isLoaded ) {
//        window.addEventListener("load", function() {
//            onDocumentReady2();
//        }, false);
//    } else {
//        onDocumentReady2();
//    }


    /////////// Gauge3 ////////////
//    function onDocumentReady3() {
//        var powerGauge = gauge('#power-gauge3', {
//            size: 200,
//            clipWidth: 210,
//            clipHeight: 120,
//            ringWidth: 50,
//            maxValue: 6,
//           // transitionMs: 4000,
//        });
//        powerGauge.render();
//
//        function updateReadings() {
//            // just pump in random data here...
//            powerGauge.update(Math.random() * 10);
//        }
//
//        // every few seconds update reading values
//        updateReadings();
//        setInterval(function() {
//            updateReadings();
//        }, 5 * 1000);
//    }
//
//    if ( !window.isLoaded ) {
//        window.addEventListener("load", function() {
//            onDocumentReady3();
//        }, false);
//    } else {
//        onDocumentReady3();
//    }



/////////// Gauge4 ////////////
//    function onDocumentReady4() {
//        var powerGauge = gauge('#power-gauge4', {
//            size: 200,
//            clipWidth: 210,
//            clipHeight: 120,
//            ringWidth: 50,
//            maxValue: 6,
//           // transitionMs: 4000,
//        });
//        powerGauge.render();
//
//        function updateReadings() {
//            // just pump in random data here...
//            powerGauge.update(1);
//        }
//
//        // every few seconds update reading values
//        updateReadings();
//    }
//
//    if ( !window.isLoaded ) {
//        window.addEventListener("load", function() {
//            onDocumentReady4();
//        }, false);
//    }